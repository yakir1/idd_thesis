\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\contentsline {chapter}{Declaration of Authorship}{iii}{section*.1}
\contentsline {chapter}{Abstract}{vii}{section*.2}
\contentsline {chapter}{Acknowledgements}{ix}{section*.3}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.13}
\contentsline {section}{\numberline {1.1}Metrics}{1}{section.14}
\contentsline {subsection}{\numberline {1.1.1}Definition}{1}{subsection.15}
\contentsline {subsection}{\numberline {1.1.2}Metrics Properties}{2}{subsection.17}
\contentsline {subsubsection}{\numberline {1.1.2.1}Non-negativity}{2}{subsubsection.18}
\contentsline {subsubsection}{\numberline {1.1.2.2}Identity of Indiscernibles}{2}{subsubsection.20}
\contentsline {subsubsection}{\numberline {1.1.2.3}Symmetry}{2}{subsubsection.22}
\contentsline {subsubsection}{\numberline {1.1.2.4}Sub-additivity (Triangle Inequality)}{2}{subsubsection.24}
\contentsline {subsection}{\numberline {1.1.3}Semi-metrics}{2}{subsection.26}
\contentsline {subsection}{\numberline {1.1.4}Pseudo-metrics}{2}{subsection.27}
\contentsline {section}{\numberline {1.2}Metric Learning}{3}{section.29}
\contentsline {section}{\numberline {1.3}Bin-to-Bin \& Cross-Bin Metrics}{3}{section.30}
\contentsline {section}{\numberline {1.4}Mahalanobis Distance}{3}{section.31}
\contentsline {section}{\numberline {1.5}Related Work}{3}{section.34}
\contentsline {section}{\numberline {1.6}Contribution}{4}{section.35}
\contentsline {chapter}{\numberline {2}ID Embedding}{5}{chapter.36}
\contentsline {section}{\numberline {2.1}Discretization}{5}{section.37}
\contentsline {section}{\numberline {2.2}Interpolation}{5}{section.38}
\contentsline {section}{\numberline {2.3}Assigning}{5}{section.41}
\contentsline {chapter}{\numberline {3}Interpolated Discretized object pairs embedding}{7}{chapter.64}
\contentsline {section}{\numberline {3.1}1D case}{7}{section.65}
\contentsline {subsection}{\numberline {3.1.1}Discretization}{7}{subsection.68}
\contentsline {subsection}{\numberline {3.1.2}Interpolation}{8}{subsection.69}
\contentsline {subsubsection}{\numberline {3.1.2.1}Interpolation Coefficients extraction - 1D}{8}{subsubsection.75}
\contentsline {subsection}{\numberline {3.1.3}Assigning}{9}{subsection.77}
\contentsline {section}{\numberline {3.2}multidimensional case}{9}{section.78}
\contentsline {subsection}{\numberline {3.2.1}Definition}{9}{subsection.79}
\contentsline {subsection}{\numberline {3.2.2}Discretization}{9}{subsection.81}
\contentsline {subsection}{\numberline {3.2.3}Interpolation}{9}{subsection.82}
\contentsline {subsubsection}{\numberline {3.2.3.1}find bounding hypercube}{10}{subsubsection.84}
\contentsline {subsubsection}{\numberline {3.2.3.2}find bounding simplex}{10}{subsubsection.85}
\contentsline {subsubsection}{\numberline {3.2.3.3}calculate sub-volumes of all simplices involved}{10}{subsubsection.87}
\contentsline {subsubsection}{\numberline {3.2.3.4}Simplex Volume Calculation}{10}{subsubsection.88}
\contentsline {subsubsection}{\numberline {3.2.3.5}Efficient method - calculating Barycentric Coordinates (BCC)}{11}{subsubsection.91}
\contentsline {subsubsection}{\numberline {3.2.3.6}O(n) solution}{11}{subsubsection.93}
\contentsline {subsubsection}{\numberline {3.2.3.7}Methods identity for n=2}{11}{subsubsection.96}
\contentsline {subsubsection}{\numberline {3.2.3.8}Volumes method}{11}{subsubsection.97}
\contentsline {subsubsection}{\numberline {3.2.3.9}Recursive method}{13}{subsubsection.105}
\contentsline {subsection}{\numberline {3.2.4}Assigning}{13}{subsection.106}
\contentsline {section}{\numberline {3.3}Non-euclidean non-embeddable Metrics example}{14}{section.131}
\contentsline {chapter}{\numberline {4}Interpolated Discretized Single objects Embedding}{19}{chapter.135}
\contentsline {section}{\numberline {4.1}Discretization}{19}{section.136}
\contentsline {section}{\numberline {4.2}Interpolation}{19}{section.138}
\contentsline {section}{\numberline {4.3}Assigning}{20}{section.139}
\contentsline {chapter}{\numberline {5}Learning}{21}{chapter.162}
\contentsline {section}{\numberline {5.1}Learning the Classification Function}{21}{section.163}
\contentsline {subsection}{\numberline {5.1.1}Efficient Stochastic Gradient Descent}{22}{subsection.167}
\contentsline {chapter}{\numberline {6}Time Complexity}{23}{chapter.174}
\contentsline {section}{\numberline {6.1}Discretization}{23}{section.175}
\contentsline {section}{\numberline {6.2}Interpolation}{23}{section.177}
\contentsline {section}{\numberline {6.3}Find the bounding hypercube}{23}{section.178}
\contentsline {section}{\numberline {6.4}Find the bounding simplex}{24}{section.179}
\contentsline {section}{\numberline {6.5}ID coefficients extraction}{24}{section.180}
\contentsline {section}{\numberline {6.6}Assigning}{24}{section.181}
\contentsline {chapter}{\numberline {7}Memory Complexity}{25}{chapter.182}
\contentsline {section}{\numberline {7.1}Definition}{25}{section.183}
\contentsline {subsection}{\numberline {7.1.1}SIFT Example}{25}{subsection.185}
\contentsline {chapter}{\numberline {8}Experiments}{27}{chapter.188}
\contentsline {section}{\numberline {8.1}experiments procedure}{27}{section.192}
\contentsline {section}{\numberline {8.2}classification solution}{28}{section.194}
\contentsline {section}{\numberline {8.3}pairs matching solution}{28}{section.195}
\contentsline {chapter}{\numberline {9}Conclusions and discussion}{29}{chapter.196}
\contentsline {chapter}{\numberline {A}Appendix Title Here}{31}{appendix.197}
